﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinhVien
{
    internal class HocVien
    {
        public int MaSinhVien {  get; set; }
        public string HoTen { get; set; }
        public int Tuoi { get; set; }
        public string Khoa { get; set; }

    }
}
